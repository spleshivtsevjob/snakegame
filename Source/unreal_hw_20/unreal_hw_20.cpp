// Copyright Epic Games, Inc. All Rights Reserved.

#include "unreal_hw_20.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, unreal_hw_20, "unreal_hw_20" );
