// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ExamplePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_HW_20_API AExamplePlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
