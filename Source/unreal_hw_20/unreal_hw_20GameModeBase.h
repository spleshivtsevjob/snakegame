// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "unreal_hw_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_HW_20_API Aunreal_hw_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
