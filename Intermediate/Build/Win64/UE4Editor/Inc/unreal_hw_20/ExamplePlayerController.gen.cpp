// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "unreal_hw_20/ExamplePlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExamplePlayerController() {}
// Cross Module References
	UNREAL_HW_20_API UClass* Z_Construct_UClass_AExamplePlayerController_NoRegister();
	UNREAL_HW_20_API UClass* Z_Construct_UClass_AExamplePlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_unreal_hw_20();
// End Cross Module References
	void AExamplePlayerController::StaticRegisterNativesAExamplePlayerController()
	{
	}
	UClass* Z_Construct_UClass_AExamplePlayerController_NoRegister()
	{
		return AExamplePlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AExamplePlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AExamplePlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_hw_20,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AExamplePlayerController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ExamplePlayerController.h" },
		{ "ModuleRelativePath", "ExamplePlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AExamplePlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AExamplePlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AExamplePlayerController_Statics::ClassParams = {
		&AExamplePlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AExamplePlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AExamplePlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AExamplePlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AExamplePlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AExamplePlayerController, 684076669);
	template<> UNREAL_HW_20_API UClass* StaticClass<AExamplePlayerController>()
	{
		return AExamplePlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AExamplePlayerController(Z_Construct_UClass_AExamplePlayerController, &AExamplePlayerController::StaticClass, TEXT("/Script/unreal_hw_20"), TEXT("AExamplePlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AExamplePlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
