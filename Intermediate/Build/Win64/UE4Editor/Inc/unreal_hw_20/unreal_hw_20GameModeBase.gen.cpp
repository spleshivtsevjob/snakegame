// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "unreal_hw_20/unreal_hw_20GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeunreal_hw_20GameModeBase() {}
// Cross Module References
	UNREAL_HW_20_API UClass* Z_Construct_UClass_Aunreal_hw_20GameModeBase_NoRegister();
	UNREAL_HW_20_API UClass* Z_Construct_UClass_Aunreal_hw_20GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_unreal_hw_20();
// End Cross Module References
	void Aunreal_hw_20GameModeBase::StaticRegisterNativesAunreal_hw_20GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_Aunreal_hw_20GameModeBase_NoRegister()
	{
		return Aunreal_hw_20GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_hw_20,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "unreal_hw_20GameModeBase.h" },
		{ "ModuleRelativePath", "unreal_hw_20GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Aunreal_hw_20GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics::ClassParams = {
		&Aunreal_hw_20GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Aunreal_hw_20GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Aunreal_hw_20GameModeBase, 245401741);
	template<> UNREAL_HW_20_API UClass* StaticClass<Aunreal_hw_20GameModeBase>()
	{
		return Aunreal_hw_20GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Aunreal_hw_20GameModeBase(Z_Construct_UClass_Aunreal_hw_20GameModeBase, &Aunreal_hw_20GameModeBase::StaticClass, TEXT("/Script/unreal_hw_20"), TEXT("Aunreal_hw_20GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Aunreal_hw_20GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
