// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREAL_HW_20_unreal_hw_20GameModeBase_generated_h
#error "unreal_hw_20GameModeBase.generated.h already included, missing '#pragma once' in unreal_hw_20GameModeBase.h"
#endif
#define UNREAL_HW_20_unreal_hw_20GameModeBase_generated_h

#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_SPARSE_DATA
#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_RPC_WRAPPERS
#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAunreal_hw_20GameModeBase(); \
	friend struct Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Aunreal_hw_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_hw_20"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_hw_20GameModeBase)


#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAunreal_hw_20GameModeBase(); \
	friend struct Z_Construct_UClass_Aunreal_hw_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Aunreal_hw_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_hw_20"), NO_API) \
	DECLARE_SERIALIZER(Aunreal_hw_20GameModeBase)


#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aunreal_hw_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_hw_20GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_hw_20GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_hw_20GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_hw_20GameModeBase(Aunreal_hw_20GameModeBase&&); \
	NO_API Aunreal_hw_20GameModeBase(const Aunreal_hw_20GameModeBase&); \
public:


#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aunreal_hw_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aunreal_hw_20GameModeBase(Aunreal_hw_20GameModeBase&&); \
	NO_API Aunreal_hw_20GameModeBase(const Aunreal_hw_20GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aunreal_hw_20GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aunreal_hw_20GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aunreal_hw_20GameModeBase)


#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_12_PROLOG
#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_SPARSE_DATA \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_RPC_WRAPPERS \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_INCLASS \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_SPARSE_DATA \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_HW_20_API UClass* StaticClass<class Aunreal_hw_20GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_hw_20_Source_unreal_hw_20_unreal_hw_20GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
