// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "unreal_hw_20/ExampleActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExampleActor() {}
// Cross Module References
	UNREAL_HW_20_API UEnum* Z_Construct_UEnum_unreal_hw_20_EExample();
	UPackage* Z_Construct_UPackage__Script_unreal_hw_20();
	UNREAL_HW_20_API UScriptStruct* Z_Construct_UScriptStruct_FMyData();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	UNREAL_HW_20_API UClass* Z_Construct_UClass_AExampleActor_NoRegister();
	UNREAL_HW_20_API UClass* Z_Construct_UClass_AExampleActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
// End Cross Module References
	static UEnum* EExample_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_unreal_hw_20_EExample, Z_Construct_UPackage__Script_unreal_hw_20(), TEXT("EExample"));
		}
		return Singleton;
	}
	template<> UNREAL_HW_20_API UEnum* StaticEnum<EExample>()
	{
		return EExample_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExample(EExample_StaticEnum, TEXT("/Script/unreal_hw_20"), TEXT("EExample"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_unreal_hw_20_EExample_Hash() { return 1844793502U; }
	UEnum* Z_Construct_UEnum_unreal_hw_20_EExample()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_unreal_hw_20();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExample"), 0, Get_Z_Construct_UEnum_unreal_hw_20_EExample_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EExample::U_RED", (int64)EExample::U_RED },
				{ "EExample::U_GREEN", (int64)EExample::U_GREEN },
				{ "EExample::U_BLUE", (int64)EExample::U_BLUE },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "ExampleActor.h" },
				{ "U_BLUE.DisplayName", "BLUE" },
				{ "U_BLUE.Name", "EExample::U_BLUE" },
				{ "U_GREEN.DisplayName", "GREEN" },
				{ "U_GREEN.Name", "EExample::U_GREEN" },
				{ "U_RED.DisplayName", "RED" },
				{ "U_RED.Name", "EExample::U_RED" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_unreal_hw_20,
				nullptr,
				"EExample",
				"EExample",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMyData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern UNREAL_HW_20_API uint32 Get_Z_Construct_UScriptStruct_FMyData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMyData, Z_Construct_UPackage__Script_unreal_hw_20(), TEXT("MyData"), sizeof(FMyData), Get_Z_Construct_UScriptStruct_FMyData_Hash());
	}
	return Singleton;
}
template<> UNREAL_HW_20_API UScriptStruct* StaticStruct<FMyData>()
{
	return FMyData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMyData(FMyData::StaticStruct, TEXT("/Script/unreal_hw_20"), TEXT("MyData"), false, nullptr, nullptr);
static struct FScriptStruct_unreal_hw_20_StaticRegisterNativesFMyData
{
	FScriptStruct_unreal_hw_20_StaticRegisterNativesFMyData()
	{
		UScriptStruct::DeferCppStructOps<FMyData>(FName(TEXT("MyData")));
	}
} ScriptStruct_unreal_hw_20_StaticRegisterNativesFMyData;
	struct Z_Construct_UScriptStruct_FMyData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumValue_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorPtr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMyData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMyData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMyData>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMyData_Statics::NewProp_EnumValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMyData_Statics::NewProp_EnumValue_MetaData[] = {
		{ "Category", "MyData" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMyData_Statics::NewProp_EnumValue = { "EnumValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMyData, EnumValue), Z_Construct_UEnum_unreal_hw_20_EExample, METADATA_PARAMS(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_EnumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_EnumValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMyData_Statics::NewProp_IntValue_MetaData[] = {
		{ "Category", "MyData" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMyData_Statics::NewProp_IntValue = { "IntValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMyData, IntValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_IntValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_IntValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMyData_Statics::NewProp_StringValue_MetaData[] = {
		{ "Category", "MyData" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMyData_Statics::NewProp_StringValue = { "StringValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMyData, StringValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_StringValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_StringValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMyData_Statics::NewProp_ActorPtr_MetaData[] = {
		{ "Category", "MyData" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMyData_Statics::NewProp_ActorPtr = { "ActorPtr", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMyData, ActorPtr), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_ActorPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMyData_Statics::NewProp_ActorPtr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMyData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMyData_Statics::NewProp_EnumValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMyData_Statics::NewProp_EnumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMyData_Statics::NewProp_IntValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMyData_Statics::NewProp_StringValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMyData_Statics::NewProp_ActorPtr,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMyData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_hw_20,
		nullptr,
		&NewStructOps,
		"MyData",
		sizeof(FMyData),
		alignof(FMyData),
		Z_Construct_UScriptStruct_FMyData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMyData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMyData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMyData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMyData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMyData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_unreal_hw_20();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MyData"), sizeof(FMyData), Get_Z_Construct_UScriptStruct_FMyData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMyData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMyData_Hash() { return 943114338U; }
	void AExampleActor::StaticRegisterNativesAExampleActor()
	{
	}
	UClass* Z_Construct_UClass_AExampleActor_NoRegister()
	{
		return AExampleActor::StaticClass();
	}
	struct Z_Construct_UClass_AExampleActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExampleEnumValue_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExampleEnumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExampleEnumValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExampleStructValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExampleStructValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AExampleActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_hw_20,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AExampleActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ExampleActor.h" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleEnumValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleEnumValue_MetaData[] = {
		{ "Category", "ExampleActor" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleEnumValue = { "ExampleEnumValue", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AExampleActor, ExampleEnumValue), Z_Construct_UEnum_unreal_hw_20_EExample, METADATA_PARAMS(Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleEnumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleEnumValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleStructValue_MetaData[] = {
		{ "Category", "ExampleActor" },
		{ "ModuleRelativePath", "ExampleActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleStructValue = { "ExampleStructValue", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AExampleActor, ExampleStructValue), Z_Construct_UScriptStruct_FMyData, METADATA_PARAMS(Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleStructValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleStructValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AExampleActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleEnumValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleEnumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AExampleActor_Statics::NewProp_ExampleStructValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AExampleActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AExampleActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AExampleActor_Statics::ClassParams = {
		&AExampleActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AExampleActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AExampleActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AExampleActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AExampleActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AExampleActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AExampleActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AExampleActor, 3781543811);
	template<> UNREAL_HW_20_API UClass* StaticClass<AExampleActor>()
	{
		return AExampleActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AExampleActor(Z_Construct_UClass_AExampleActor, &AExampleActor::StaticClass, TEXT("/Script/unreal_hw_20"), TEXT("AExampleActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AExampleActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
