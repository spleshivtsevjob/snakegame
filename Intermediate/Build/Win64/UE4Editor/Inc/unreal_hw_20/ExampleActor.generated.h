// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREAL_HW_20_ExampleActor_generated_h
#error "ExampleActor.generated.h already included, missing '#pragma once' in ExampleActor.h"
#endif
#define UNREAL_HW_20_ExampleActor_generated_h

#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMyData_Statics; \
	UNREAL_HW_20_API static class UScriptStruct* StaticStruct();


template<> UNREAL_HW_20_API UScriptStruct* StaticStruct<struct FMyData>();

#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_SPARSE_DATA
#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_RPC_WRAPPERS
#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_RPC_WRAPPERS_NO_PURE_DECLS
#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAExampleActor(); \
	friend struct Z_Construct_UClass_AExampleActor_Statics; \
public: \
	DECLARE_CLASS(AExampleActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_hw_20"), NO_API) \
	DECLARE_SERIALIZER(AExampleActor)


#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_INCLASS \
private: \
	static void StaticRegisterNativesAExampleActor(); \
	friend struct Z_Construct_UClass_AExampleActor_Statics; \
public: \
	DECLARE_CLASS(AExampleActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_hw_20"), NO_API) \
	DECLARE_SERIALIZER(AExampleActor)


#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AExampleActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AExampleActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExampleActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExampleActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExampleActor(AExampleActor&&); \
	NO_API AExampleActor(const AExampleActor&); \
public:


#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExampleActor(AExampleActor&&); \
	NO_API AExampleActor(const AExampleActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExampleActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExampleActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AExampleActor)


#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_PRIVATE_PROPERTY_OFFSET
#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_45_PROLOG
#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_PRIVATE_PROPERTY_OFFSET \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_SPARSE_DATA \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_RPC_WRAPPERS \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_INCLASS \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_PRIVATE_PROPERTY_OFFSET \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_SPARSE_DATA \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_INCLASS_NO_PURE_DECLS \
	unreal_hw_20_Source_unreal_hw_20_ExampleActor_h_48_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_HW_20_API UClass* StaticClass<class AExampleActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_hw_20_Source_unreal_hw_20_ExampleActor_h


#define FOREACH_ENUM_EEXAMPLE(op) \
	op(EExample::U_RED) \
	op(EExample::U_GREEN) \
	op(EExample::U_BLUE) 

enum class EExample : uint8;
template<> UNREAL_HW_20_API UEnum* StaticEnum<EExample>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
