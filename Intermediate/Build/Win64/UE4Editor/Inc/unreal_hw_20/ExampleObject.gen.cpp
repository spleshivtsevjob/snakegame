// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "unreal_hw_20/ExampleObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExampleObject() {}
// Cross Module References
	UNREAL_HW_20_API UClass* Z_Construct_UClass_UExampleObject_NoRegister();
	UNREAL_HW_20_API UClass* Z_Construct_UClass_UExampleObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_unreal_hw_20();
// End Cross Module References
	void UExampleObject::StaticRegisterNativesUExampleObject()
	{
	}
	UClass* Z_Construct_UClass_UExampleObject_NoRegister()
	{
		return UExampleObject::StaticClass();
	}
	struct Z_Construct_UClass_UExampleObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExampleObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_unreal_hw_20,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExampleObject_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "ExampleObject.h" },
		{ "ModuleRelativePath", "ExampleObject.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExampleObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExampleObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExampleObject_Statics::ClassParams = {
		&UExampleObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UExampleObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UExampleObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExampleObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExampleObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExampleObject, 603154762);
	template<> UNREAL_HW_20_API UClass* StaticClass<UExampleObject>()
	{
		return UExampleObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExampleObject(Z_Construct_UClass_UExampleObject, &UExampleObject::StaticClass, TEXT("/Script/unreal_hw_20"), TEXT("UExampleObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExampleObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
