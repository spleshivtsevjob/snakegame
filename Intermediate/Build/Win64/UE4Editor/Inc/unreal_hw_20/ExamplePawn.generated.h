// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREAL_HW_20_ExamplePawn_generated_h
#error "ExamplePawn.generated.h already included, missing '#pragma once' in ExamplePawn.h"
#endif
#define UNREAL_HW_20_ExamplePawn_generated_h

#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_SPARSE_DATA
#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_RPC_WRAPPERS
#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAExamplePawn(); \
	friend struct Z_Construct_UClass_AExamplePawn_Statics; \
public: \
	DECLARE_CLASS(AExamplePawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_hw_20"), NO_API) \
	DECLARE_SERIALIZER(AExamplePawn)


#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAExamplePawn(); \
	friend struct Z_Construct_UClass_AExamplePawn_Statics; \
public: \
	DECLARE_CLASS(AExamplePawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/unreal_hw_20"), NO_API) \
	DECLARE_SERIALIZER(AExamplePawn)


#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AExamplePawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AExamplePawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExamplePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExamplePawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExamplePawn(AExamplePawn&&); \
	NO_API AExamplePawn(const AExamplePawn&); \
public:


#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExamplePawn(AExamplePawn&&); \
	NO_API AExamplePawn(const AExamplePawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExamplePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExamplePawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AExamplePawn)


#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_PRIVATE_PROPERTY_OFFSET
#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_9_PROLOG
#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_SPARSE_DATA \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_RPC_WRAPPERS \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_INCLASS \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_PRIVATE_PROPERTY_OFFSET \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_SPARSE_DATA \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_INCLASS_NO_PURE_DECLS \
	unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREAL_HW_20_API UClass* StaticClass<class AExamplePawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID unreal_hw_20_Source_unreal_hw_20_ExamplePawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
